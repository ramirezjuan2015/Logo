import React, { useContext } from "react";
import { View, StyleSheet, TouchableOpacity } from "react-native";
import { Button, Text, ListItem } from "react-native-elements";
import { NavigationEvents } from "react-navigation";
import { Context as locContext } from "../context/locContext";
import Spacer from "../Componentes/Spacer";
import { MaterialCommunityIcons } from '@expo/vector-icons';

const GuardarLocacion = ({ navigation }) => {
  const { state, got } = useContext(locContext);
  const animalId = navigation.getParam('animalId');
  const locations = state.filter(l => l.animalId === animalId);

  console.log('state', state);
  console.log(animalId);
  console.log(locations);

  const { container, opacityStyle, materialStyle } = style
  return (
    <>
      <NavigationEvents onWillFocus={got} />

      <View style={container}>

        {locations.map(location =>

          <TouchableOpacity style={opacityStyle} onPress={() => navigation.navigate
            ('GuardarMapa', { locationId: location._id, location })}>
            <ListItem chevron title={location.name} />
            < MaterialCommunityIcons style={materialStyle}
              name="delete" size={24} color="black" />
          </TouchableOpacity>
        )
        }
        <Spacer>
          <Button
            buttonStyle={{ backgroundColor: 'orange', height: 50, borderRadius: 10 }}
            title="Crea una locación"
            onPress={() => navigation.navigate('Location',
              { animalId })} />
        </Spacer>

      </View>
    </>
  );
};

GuardarLocacion.navigationOptions = {
  title: "Navegar",
};

const style = StyleSheet.create({
  container: {
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10
  },
  opacityStyle: {
    marginRight: 10,
    marginLeft: 10,
    marginTop: 5,
  },
  materialStyle: {
    marginStart: 260,
  }
});

export default GuardarLocacion; 
